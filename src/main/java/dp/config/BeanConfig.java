package dp.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

@Configuration
public class BeanConfig {

    @Bean
    public DataSource dataSource(@Value("${mysql.driverClassName}") String driverClass,
                                 @Value("${mysql.url}") String url,
                                 @Value("${mysql.user}") String user,
                                 @Value("${mysql.pass}") String pass,
                                 @Value("${mysql.testConnection}") boolean testConnection) throws PropertyVetoException {
        ComboPooledDataSource ds = new ComboPooledDataSource();
        ds.setDriverClass(driverClass);
        ds.setJdbcUrl(url);
        ds.setUser(user);
        ds.setPassword(pass);
        ds.setTestConnectionOnCheckout(testConnection);
        return ds;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}
