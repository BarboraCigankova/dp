package dp.resources;

import dp.categorization.Categorization;
import dp.dao.LastSyncDAO;
import dp.dto.Category;
import dp.keyWords.KeyWordsGetter;
import dp.services.Syncer;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Component
@Path("")
public class CrowdInterfaceResource {
    private final Categorization categorization;
    private final LastSyncDAO lastSyncDAO;
    private final KeyWordsGetter keyWordsGetter;
    private final Syncer syncer;

    @Autowired
    public CrowdInterfaceResource(Categorization categorization, LastSyncDAO lastSyncDAO, KeyWordsGetter keyWordsGetter, Syncer syncer) {
        this.categorization = categorization;
        this.lastSyncDAO = lastSyncDAO;
        this.keyWordsGetter = keyWordsGetter;
        this.syncer = syncer;
    }

    @POST
    @Path("/categorize/{caseStudyNumb}")
    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
    @Produces(MediaType.APPLICATION_JSON)
    public String categorize(@PathParam("caseStudyNumb") int caseStudyNumb, byte[] bytes) throws IOException, SQLException {
        String text = new String(bytes, StandardCharsets.UTF_8);
        text = text.replace("text=", "");
        text = text.replaceAll("\\+", " ");

        Category category = categorization.categorize(text, caseStudyNumb);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("categoryName", category.getName());
        return jsonObject.toString();
    }

    @GET
    @Path("/keyWords/{caseStudyNumb}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getKeyWords(@PathParam("caseStudyNumb") int caseStudyNumb) throws SQLException {
        List<String> keyWords = keyWordsGetter.getKeyWords(caseStudyNumb);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("keyWords", keyWords);
        return jsonObject.toString();
    }

    @GET
    @Path("/lastSync")
    @Produces(MediaType.APPLICATION_JSON)
    public Date getLastSync() {
        Date lastSync = lastSyncDAO.getLastSync();
        if (lastSync == null) {
            return Date.from(Instant.EPOCH);
        }

        return lastSync;
    }

    @POST
    @Path("/syncData")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sync(String json) {
        JSONObject data = new JSONObject(json);
        if (Syncer.isSyncReady()) {
            syncer.sync(data);
            return Response.ok().build();
        }

        return Response.status(503).entity("Sync is not ready now.").build();
    }

}
