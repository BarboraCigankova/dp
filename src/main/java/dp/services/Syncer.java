package dp.services;

import dp.dao.CategoryDAO;
import dp.dao.DocumentDAO;
import dp.dao.LastSyncDAO;
import dp.dto.Category;
import dp.dto.Document;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class Syncer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Syncer.class);
    private static AtomicBoolean syncReady = new AtomicBoolean(true);
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final CategoryDAO categoryDAO;
    private final DocumentDAO documentDAO;
    private final NgramsImporter ngramsImporter;
    private final KeyWordsImporter keyWordsImporter;
    private final LastSyncDAO lastSyncDAO;

    @Autowired
    public Syncer(CategoryDAO categoryDAO,
                  DocumentDAO documentDAO,
                  NgramsImporter ngramsImporter,
                  KeyWordsImporter keyWordsImporter,
                  LastSyncDAO lastSyncDAO) {
        this.categoryDAO = categoryDAO;
        this.documentDAO = documentDAO;
        this.ngramsImporter = ngramsImporter;
        this.keyWordsImporter = keyWordsImporter;
        this.lastSyncDAO = lastSyncDAO;
    }

    public static boolean isSyncReady() {
        return syncReady.get();
    }

    public void sync(JSONObject data) {
        scheduler.execute(() -> {
            try {
                syncReady.set(false);

                int caseStudyNumb = data.getInt("caseStudyNumb");
                JSONArray objects = data.getJSONArray("objects");

                LOGGER.info("Store documents.");
                for (Object object : objects) {
                    JSONObject jsonObject = (JSONObject) object;
                    int postId = jsonObject.getInt("postId");
                    String postTitle = jsonObject.getString("postTitle");
                    String postContent = jsonObject.getString("postContent");
                    int categoryId = jsonObject.getInt("categoryId");
                    String categoryName = jsonObject.getString("categoryName").toLowerCase();
                    boolean keywordsUse = jsonObject.isNull("keywordsUse") || jsonObject.getBoolean("keywordsUse");
                    String expectedCategory = jsonObject.getString("expectedCategory");

                    Integer dbCategoryId = categoryDAO.getCategoryId(categoryName);
                    if (dbCategoryId == -1) {
                        dbCategoryId = categoryDAO.putCategory(new Category(categoryName, caseStudyNumb));
                    }

                    Integer expectedCategoryDb = categoryDAO.getCategoryId(expectedCategory);

                    Document document = documentDAO.getDocument(postId, "WP");
                    if (document == null) {
                        Document newDoc = new Document(postId, "WP", postContent, dbCategoryId);
                        newDoc.setKeyWordsUse(keywordsUse);
                        if(expectedCategoryDb!= -1 ){
                            newDoc.setExpectedCategory(new Category(expectedCategoryDb, expectedCategory, caseStudyNumb));
                        }

                        documentDAO.putDocument(newDoc);
                    } else {
                        document.setText(postContent);
                        document.setCategoryId(dbCategoryId);
                        document.setKeyWordsUse(keywordsUse);
                        if(expectedCategoryDb!= -1 ){
                            document.setExpectedCategory(new Category(expectedCategoryDb, expectedCategory, caseStudyNumb));
                        }
                        documentDAO.updateDocument(document);
                    }
                }

                LOGGER.info("Calculate N-grams.");
                ngramsImporter.importNgrams(caseStudyNumb);

                LOGGER.info("Calculate keywords.");
                keyWordsImporter.importKeyWords(caseStudyNumb);

                lastSyncDAO.putLastSync(Date.from(Instant.now()));
                syncReady.set(true);
            } catch (Throwable e) {
                syncReady.set(true);
                LOGGER.error("Error while sync data - {}", e);
                throw new RuntimeException(e);
            }

            LOGGER.info("Sync done.");
        });
    }
}
