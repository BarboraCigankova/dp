package dp.dto;

import java.util.List;

public class Category {
    private int id;
    private String name;
    private List<String> profile;
    private List<String> keyWords;
    private int caseStudyNumb;

    public Category(int id, String name,int caseStudyNumb) {
        this.id = id;
        this.name = name;
        this.caseStudyNumb = caseStudyNumb;
    }

    public Category(String name, int caseStudyNumb) {
        this.name = name;
        this.caseStudyNumb = caseStudyNumb;
    }

    public Category() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getProfile() {
        return profile;
    }

    public void setProfile(List<String> profile) {
        this.profile = profile;
    }

    public List<String> getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(List<String> keyWords) {
        this.keyWords = keyWords;
    }

    public int getCaseStudyNumb() {
        return caseStudyNumb;
    }

    public void setCaseStudyNumb(int caseStudyNumb) {
        this.caseStudyNumb = caseStudyNumb;
    }
}
