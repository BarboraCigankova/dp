package dp.dao;

import com.google.common.base.Preconditions;
import dp.dto.Category;
import dp.mappers.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class CategoryDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CategoryDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Category> getCategories(int caseStudyNumb) {
        return jdbcTemplate.query("select * from category where case_study_numb = ?", new Object[]{caseStudyNumb}, new CategoryMapper());
    }

    public Integer putCategory(Category category) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement pst = con.prepareStatement("insert INTO category(name, case_study_numb) values (?, ?)", new String[]{"id"});
                        pst.setString(1, category.getName());
                        pst.setInt(2, category.getCaseStudyNumb());
                        return pst;
                    }
                },
                keyHolder);
        return keyHolder.getKey().intValue();
    }

    public Integer getCategoryId(String name) {
        List<Category> category = jdbcTemplate.query("select * from category where name = ?", new Object[]{name}, new CategoryMapper());
        Preconditions.checkArgument(category.size() <= 1);
        return category.isEmpty() ? -1 : category.get(0).getId();
    }
}
