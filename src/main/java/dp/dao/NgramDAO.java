package dp.dao;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class NgramDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(NgramDAO.class);
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public NgramDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<String> getNgramsOfProfile(int categoryId) {
        Map<String, Integer> result = Maps.newLinkedHashMap();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from category_profile_item where category_id = " + categoryId);
        for (Map<String, Object> row : rows) {
            Integer ngram_id = (Integer) row.get("ngram_id");
            Integer position = (Integer) row.get("position");

            List<Map<String, Object>> rowsNgram = jdbcTemplate.queryForList("select * from ngram where ngram_id = " + ngram_id);
            for (Map<String, Object> rowNgram : rowsNgram) {
                String ngram = (String) rowNgram.get("ngram");
                result.put(ngram, position);
            }
        }

        //sort by position
        result = result.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        return Lists.newArrayList(result.keySet());
    }

    public void putNgramsPerCategory(Integer categoryId, List<String> ngrams) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.withTableName("ngram").usingColumns("ngram").usingGeneratedKeyColumns("ngram_id");
        Map<String, Object> parameters = Maps.newHashMap();

        Map<Integer, Integer> ngramIdsPosition = Maps.newLinkedHashMap();
        for (int i = 0; i < ngrams.size(); i++) {
            try {
                parameters.put("ngram", ngrams.get(i));

                Number key = jdbcInsert.executeAndReturnKey(new MapSqlParameterSource(parameters));
                ngramIdsPosition.put(((Number) key).intValue(), i + 1);
            } catch (DuplicateKeyException e) {
                LOGGER.debug("Existing ngram: {}", ngrams.get(i));
                ngramIdsPosition.put(getNgramId(ngrams.get(i)), i + 1);
            }
        }


        for (Map.Entry<Integer, Integer> entry : ngramIdsPosition.entrySet()) {
            jdbcTemplate.update("insert INTO category_profile_item(category_id, ngram_id, position) values (?, ?, ?)",
                    categoryId, entry.getKey(), entry.getValue());
        }
    }

    public void deleteProfileOfCategory(int categoryId) {
        jdbcTemplate.update("delete from category_profile_item where category_id = '" + categoryId + "'");
    }

    private Integer getNgramId(String ngram) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from ngram where ngram = '" + ngram + "'");
        if(!rows.isEmpty()){
            return (Integer) rows.get(0).get("ngram_id");
        } else {
            throw new IllegalArgumentException("Ngram " + ngram + " doesn't exists.");
        }
    }
}
