package dp.dao;

import com.google.common.base.Preconditions;
import dp.dto.Document;
import dp.mappers.DocumentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DocumentDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DocumentDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Document> getDocuments() {
        return jdbcTemplate.query("select * from document", new DocumentMapper());
    }

    public Document getDocument(int id, String source) {
        List<Document> docs = jdbcTemplate.query("select * from document where id = ? and source = ?", new Object[]{id, source}, new DocumentMapper());
        Preconditions.checkArgument(docs.size() <= 1);
        return docs.isEmpty() ? null : docs.get(0);
    }

    public void putDocument(Document document) {
        if (document.getId() != null) {
            jdbcTemplate.update("insert INTO document(id, text, source, category_id) values (?, ?, ?, ?)",
                    document.getId(), document.getText(), document.getSource(), document.getCategoryId());
        } else {
            jdbcTemplate.update("insert INTO document(text, source, category_id) values (?, ?, ?)",
                    document.getText(), document.getSource(), document.getCategoryId());
        }

        if (document.getExpectedCategory() != null && document.isKeyWordsUse() != null) {
            jdbcTemplate.update("insert INTO documentmeta(documentId, expectedCategory, keywordsUse) values (?, ?, ?)",
                    document.getId(), document.getExpectedCategory().getId(), document.isKeyWordsUse());
        }
    }

    public List<Document> getDocumentByCategory(int categoryId) {
        return jdbcTemplate.query("select * from document where category_id = ?", new Object[]{categoryId}, new DocumentMapper());
    }

    public int getDocumentByText(String text) {
        List<Document> docs = jdbcTemplate.query("select * from document where text = ?", new Object[]{text}, new DocumentMapper());
        Preconditions.checkArgument(docs.size() <= 1);

        if (docs.isEmpty()) {
            return -1;
        }
        return docs.get(0).getId();
    }

    public void updateDocument(Document document) {
        jdbcTemplate.update("update document set category_id = ?, text = ? where id = ? and source = ?",
                document.getCategoryId(), document.getText(), document.getId(), document.getSource());

        if (document.getExpectedCategory() != null && document.isKeyWordsUse() != null) {
            jdbcTemplate.update("update documentmeta set expectedCategory=?, keywordsUse=? values where documentId = ?",
                    document.getExpectedCategory().getId(), document.isKeyWordsUse(), document.getId());
        }
    }
}
