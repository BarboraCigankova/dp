package dp.dao;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class KeyWordsDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public KeyWordsDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void putKeyWords(Map<Integer, List<String>> keyWordsPerCategory) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.withTableName("keyword").usingColumns("word").usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = Maps.newHashMap();

        for (Map.Entry<Integer, List<String>> entry : keyWordsPerCategory.entrySet()) {
            for (String keyWord : entry.getValue()) {
                int keyWordId = getKeyWordIdByName(keyWord);

                if (keyWordId == -1) {
                    parameters.put("word", keyWord);
                    Number key = jdbcInsert.executeAndReturnKey(new MapSqlParameterSource(parameters));
                    keyWordId = ((Number) key).intValue();
                }

                jdbcTemplate.update("insert INTO keyword_category(category_id, keyWord_id) values (?, ?)",
                        entry.getKey(), keyWordId);
            }
        }
    }

    public Map<Integer, List<String>> getKeyWords(List<Integer> categoryIds) {
        Map<Integer, List<String>> result = Maps.newLinkedHashMap();

        for (Integer categoryId : categoryIds) {
            List<String> keyWordsForCategory = getKeyWordsForCategory(categoryId);
            result.put(categoryId, keyWordsForCategory);
        }

        return result;
    }

    private List<String> getKeyWordsForCategory(Integer categoryId) {
        List<String> result = Lists.newArrayList();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from keyword_category where category_id = " + categoryId);
        for (Map<String, Object> row : rows) {
            Integer keyWordsId = (Integer) row.get("keyWord_id");

            List<Map<String, Object>> rowsKeyword = jdbcTemplate.queryForList("select * from keyword where id = " + keyWordsId);
            for (Map<String, Object> rowKeyword : rowsKeyword) {
                String word = (String) rowKeyword.get("word");

                result.add(word);
            }
        }

        return result;
    }

    public void deleteKeyWordsByCatId(int categoryId) {
        jdbcTemplate.update("delete from keyword_category where category_id = " + categoryId);
    }

    private Integer getKeyWordIdByName(String keyWord) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select id from keyword where word = '" + keyWord + "'");
        Preconditions.checkArgument(rows.isEmpty() || rows.size() == 1);

        if(!rows.isEmpty()){
            return (Integer) rows.get(0).get("id");
        }
        return -1;
    }
}
