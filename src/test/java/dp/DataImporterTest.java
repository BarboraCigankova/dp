package dp;

import com.mysql.cj.jdbc.MysqlDataSource;
import dp.dao.CategoryDAO;
import dp.dao.DocumentDAO;
import dp.dao.KeyWordsDAO;
import dp.dao.NgramDAO;
import dp.keyWords.TfIdfCalculator;
import dp.services.DataImporter;
import dp.services.DocumentsImporter;
import dp.services.KeyWordsImporter;
import dp.services.NgramsImporter;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.IOException;
import java.sql.SQLException;

public class DataImporterTest {
    private DataImporter dataImporter;

    @Before
    public void init() {
        MysqlDataSource ds = new MysqlDataSource();
        ds.setURL("jdbc:mysql://127.0.0.1/dp?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC");
        ds.setUser("root");
        ds.setPassword("vertrigo");

        JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
        CategoryDAO categoryDAO = new CategoryDAO(jdbcTemplate);
        DocumentDAO documentDAO = new DocumentDAO(jdbcTemplate);
        NgramDAO ngramDAO = new NgramDAO(jdbcTemplate);
        NgramsImporter ngramsImporter = new NgramsImporter(categoryDAO, documentDAO, ngramDAO);
        KeyWordsDAO keyWordsDAO = new KeyWordsDAO(jdbcTemplate);
        KeyWordsImporter keyWordsImporter = new KeyWordsImporter(new TfIdfCalculator(documentDAO, categoryDAO), keyWordsDAO);

        this.dataImporter = new DataImporter(ngramsImporter,
                new DocumentsImporter(categoryDAO, documentDAO), keyWordsImporter);
    }

    @Ignore
    @Test
    public void shouldLoadMalciksTexts() throws IOException, SQLException {
        dataImporter.importData("C:\\Users\\Barča\\Documents\\VŠ\\DP\\texty2\\train", 1, false);
    }

    @Ignore
    @Test
    public void shouldLoadOsuTexts() throws IOException, SQLException {
        dataImporter.importData("C:\\Users\\Barča\\Documents\\VŠ\\DP\\texty3\\train", 2, false);
    }

    @Ignore
    @Test
    public void shouldLoadLanguagesTexts() throws IOException, SQLException {
        dataImporter.importData("C:\\Users\\Barča\\Documents\\VŠ\\DP\\texty4\\train", 3, false);
    }

    @Ignore
    @Test
    public void shouldLoadMalciksTexts2() throws IOException, SQLException {
        dataImporter.importData("C:\\Users\\Barča\\Documents\\VŠ\\DP\\texty1\\train", 4, false);
    }

    @Ignore
    @Test
    public void shouldLoadSentimentSentences() throws IOException, SQLException {
        dataImporter.importData("C:\\Users\\Barča\\Documents\\VŠ\\DP\\texty5\\train", 5, false);
    }
}
